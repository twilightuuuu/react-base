const webpack = require('webpack');
const path = require('path');

module.exports = {
    entry: path.resolve(__dirname, './src/index.js'), // where webpack starts bundling
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/, // use the loader on .js files
                exclude: /node_modules/, // exclude the node_modules directory
                use: ['babel-loader'] // what loader to use
            }
        ]
    },
    resolve: { // options for resolving module requests
        extensions: ['*', '.js', 'jsx'] // used extensions
    },
    output: { // options for how webpack emits results
        path: path.resolve(__dirname, './dist'), // directory for all output files
        filename: 'bundle.js' // filename template for entry chunks
    },
    plugins: [ // list of plugins to use
        new webpack.HotModuleReplacementPlugin() // apply source code changes without reloading the page
    ],
    devServer: {
        contentBase: path.resolve(__dirname, './dist') // static file directory location
    }
};
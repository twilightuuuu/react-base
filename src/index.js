import React from 'react';
import ReactDOM from 'react-dom';

import('./another');
import App from './App';

console.log('hello project.');
console.log(1_000_000);

document.getElementById('button_test').addEventListener('click', elem => {
    console.log('clicked');
});

const title = 'React with Webpack and Babel';

ReactDOM.render(
    <App title={title} />,
    document.getElementById('app')
);

module.hot.accept();